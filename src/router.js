import express from 'express'
import ValidationEntries from './middleWare';
import ValidationUsers from './middleWare2';
import jwt from 'jsonwebtoken';
import jwtVerify from './jwtVerify';

const { v4: uuidv4 } = require('uuid');


let users = [{
    "id": "a3e19c87-eccd-4c0a-b491-343fbe38a8b6",
    "name": "Cleyton Greystoke",
    "password": "12ea5e4e",
    "email": "cleytongreystoke@gmail.com"
},
{
    "id": "03b4618b-ea9e-4d5e-af81-35881c616a43",
    "name": "Jane Porter",
    "password": "s56s5a1a5d",
    "email": "janeporter@gmail.com"
},
{
    "id": "c6d26e3d-68cf-43fa-bc73-f6983642a829",
    "name": "John Smith",
    "password": "a21g5e4t5a",
    "email": "johnsmith@gmail.com"
}];
let entries = [{
    "id": "c6d26e3d-68cf-43fa-bc73-f6983642a829",
    "name": "John Smith",
    "email": "johnsmith@gmail.com",
    "phoneNumber": "236-456-5555",
    "content": "Private John added"
},
{
    "id": "05d0d4a6-4f24-4257-818c-5f00178ea363",
    "name": "Jane Porter",
    "email": "janeporter@gmail.com",
    "phoneNumber": "222-2222-2222",
    "content": "Private Jane joining"
},
{
    "id": "ae33320f-9240-4fcc-ace3-511736e68842",
    "name": "Cleyton Greystoke",
    "email": "cleytongreystoke@gmail.com",
    "phoneNumber": "333-333-3333",
    "content": "Private Cleyton reporting for duty"
}];

const router = express.Router()

//router.get(helloWorld); //requires nothing responde with "Hello World" Why this does not work?
router.get('/', (req, res) => res.send('Hello world'));



router.post('/contact_form/entries', ValidationEntries, (req,res,next)=>{
   
    
    next()
    
    let newEntrie = {
        "id": uuidv4(),
        "name": req.body.name,
        "email": req.body.email,
        "phoneNumber": req.body.phoneNumber,
        "content": req.body.content 
    }


    entries.push(req.body)

    return res.status(201).json(newEntrie)
})




router.post('/users', ValidationUsers, (req,res,next)=>{
    next()
    


        let newUser = {
            "id": uuidv4(),
            "name": req.body.name,
            "password": req.body.password,
            "email": req.body.email,
            }
    

            
        users.push(req.body)

return res.status(201).json(newUser)

})


router.get('/contact_form/entries', jwtVerify, (req, res)=>{

    res.status(200).json(entries)
})


router.get('/contact_form/entries/:id', jwtVerify, (req, res)=>{

    if(req.body.id != req.params.id){
        return res.status(400).json(`Bad request id URL and request doesnt match `)
    }
   

    if(entries.find(a=> a.id == req.params.id)==undefined||!entries.find(a=> a.id == req.params.id)==true){

        return res.status(404).send(`{
            "message": "entry ${req.body.id} not found"
        }`)
        
    }

    let obj= entries.find(a=> a.id == req.params.id) 

    let index=entries.findIndex(a=> a.id==obj.id)

    
    return res.status(200).send(entries[index])
    


    
})



router.post('/auth', (req,res)=>{

    let logUser = {
        "email": req.body.email,
        "password": req.body.password
    }

    let obj = users.find(a=> a.email == req.body.email) 

    if(!obj ){
        return res.status(404).send(`User ${req.body.email} not found`)
    }

    if(obj.password!=req.body.password){
        return res.status(401).send(`{
            "message": "incorrect credentials provided"
        }`)
    }

    let token = jwt.sign(logUser, "shhhh")

    return res.status(200).json({token})

})





/*router.get('/contact_form/entries', (req,res)=>{

    return res.status(200).send(entries)
})*/

router.get("*", (req,res,next)=>{
   next(err)
})

export default router
