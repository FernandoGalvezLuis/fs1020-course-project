

let validationUsers = (req,res, next) =>{
    const reqProp = ['name', 'password', 'email']

    let missProp = []

    reqProp.forEach(prop => {
        if (!req.body.hasOwnProperty(prop)) {
            missProp.push(prop)
        }
    })



    if (missProp.length) { 
        let message = []
        missProp.forEach(prop => {
            message.push(prop)
        })


        
        return res.status(400).send(`{
            "message": "validation error",
            "invalid": "${message}"
        }`)
    }

    next()

}
export default ValidationUsers;
