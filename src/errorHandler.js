let errorHandler = (err, req, res, next)=>{

    if (res.headerSent){
        return next(err)
    }

    res.status(404).send({message: "not found"})
}

export default errorHandler;